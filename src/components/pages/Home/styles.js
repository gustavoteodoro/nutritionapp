import styled from 'styled-components';
import { Primary } from '../../bosons/colors';

export const HomeContainer = styled.div`
  width: 100%;
  min-height: 100vh;
  background: ${Primary};
  overflow: hidden;
`;
