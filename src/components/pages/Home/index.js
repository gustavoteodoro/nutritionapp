import React, { Component } from 'react';
import { shape, string } from 'prop-types';
import { connect } from 'react-redux';
import Header from '../../organisms/Header';
import FoodInfo from '../../organisms/FoodInfo';

import { HomeContainer } from './styles';

class Home extends Component {
  render() {
    const {
      food,
    } = this.props;

    return (
      <HomeContainer>
        <Header />
        {food.food_name && <FoodInfo />}
      </HomeContainer>
    );
  }
}

Home.propTypes = {
  food: shape({
    food_name: string,
  }),
};

Home.defaultProps = {
  food: {
    food_name: null,
  },
};


const mapStateToProps = state => ({
  food: state.food,
});

export default connect(mapStateToProps)(Home);
