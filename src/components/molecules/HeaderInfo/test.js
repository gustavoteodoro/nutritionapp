import React from 'react';
import HeaderInfo from './index';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer
    .create(<HeaderInfo />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});