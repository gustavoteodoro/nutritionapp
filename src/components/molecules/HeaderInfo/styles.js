import styled from 'styled-components';

import { Roboto } from '../../bosons/fonts';

export const HeaderInfoContainer = styled.header`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const HeaderInfoTitle = styled.h1`
  font-family: ${Roboto};
  font-weight: 600;
  color: white;
  font-size: 20px;
`;
