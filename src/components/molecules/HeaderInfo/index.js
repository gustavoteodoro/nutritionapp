import React, { Component } from 'react';
import Logo from '../../atoms/Logo';

import { HeaderInfoContainer, HeaderInfoTitle } from './styles';

import { title } from './data.json';

class HeaderInfo extends Component {
  render() {
    return (
      <HeaderInfoContainer>
        <Logo />
        <HeaderInfoTitle>
          {title}
        </HeaderInfoTitle>
      </HeaderInfoContainer>
    );
  }
}

export default HeaderInfo;
