import React, { Component } from 'react';
import { connect } from 'react-redux';
import { func } from 'prop-types';
import { setFood, clearFood } from '../../../actions/food';
import { getNutrients } from '../../../services/getNutrients';
import Input from '../../atoms/Input';
import Button from '../../atoms/Button';

import { SearchFormContainer, FormError } from './styles';

import { inputLabel, buttonLabel } from './data.json';

class SearchForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputFoodValue: '',
      loading: false,
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    const {
      inputFoodValue,
    } = this.state;
    if (inputFoodValue !== '') {
      this.setState({
        error: null,
        loading: true,
      });
      getNutrients(inputFoodValue)
        .then(res => res.json())
        .then((result) => {
          if (result.foods) {
            this.props.setFood(result.foods[0]);
            this.setState({
              inputFoodValue: '',
              loading: false,
            });
          } else if (result.message) {
            this.props.clearFood();
            this.setState({
              error: result.message,
              loading: false,
            });
          }
        });
    }
  }

  render() {
    const {
      inputFoodValue,
      loading,
      error,
    } = this.state;

    return (
      <SearchFormContainer onSubmit={() => this.handleSubmit()}>
        <Input
          label={inputLabel}
          onChange={e => this.setState({ inputFoodValue: e.target.value })}
          value={inputFoodValue}
        />
        <Button label={buttonLabel} onClick={e => this.handleSubmit(e)} loading={loading} />
        {error && (
        <FormError>
          {error}
        </FormError>
        )}
      </SearchFormContainer>
    );
  }
}

SearchForm.propTypes = {
  setFood: func,
  clearFood: func,
};

SearchForm.defaultProps = {
  setFood: null,
  clearFood: null,
};

const mapStateToProps = state => ({
  food: state.food,
});

const mapDispatchToProps = dispatch => ({
  setFood(food) {
    dispatch(setFood(food));
  },
  clearFood() {
    dispatch(clearFood());
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);
