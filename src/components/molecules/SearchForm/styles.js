import styled from 'styled-components';

import { Roboto } from '../../bosons/fonts';
import { media } from '../../bosons/media';

export const SearchFormContainer = styled.form`
  display: flex;
  position: relative;
  width: 100%;
  box-sizing: border-box;
  max-width: 600px;
  background: white;
  border-radius: 5px;
  padding: 20px;

  ${media.mobile`
    border-radius: 0;
    flex-direction: column;
  `}
`;

export const FormError = styled.span`
  display: block;
  position: absolute;
  font-family: ${Roboto};
  bottom: -20px;
  font-size: 12px;
  left: 50%;
  transform: translateX(-50%);
  color: white;

  ${media.mobile`
    bottom: -40px;
  `}
`;
