import React from 'react';
import FoodAspect from './index';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer
    .create(<FoodAspect value={12} label="Calories" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});