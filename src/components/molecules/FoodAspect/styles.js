import styled from 'styled-components';

import { Secondary } from '../../bosons/colors';
import { Roboto } from '../../bosons/fonts';
import { media } from '../../bosons/media';

export const FoodAspectContainer = styled.div`
  display: flex;
  width: 20%;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 20px 0;

  ${media.mobile`
    width: 33%;
  `}
`;

export const FoodAspectValue = styled.span`
  font-family: ${Roboto};
  color: ${Secondary};
  font-size: 25px;
  margin-bottom: 10px;
`;

export const FoodAspectLabel = styled.span`
  font-family: ${Roboto};
  font-size: 12px;
`;
