import styled from 'styled-components';
import { Primary } from '../../bosons/colors';
import { Roboto } from '../../bosons/fonts';
import { media } from '../../bosons/media';

export const InputElement = styled.input`
  width: 80%;
  box-sizing: border-box;
  height: 40px;
  font-family: ${Roboto};
  color: ${Primary};
  border: 2px solid ${Primary};
  font-size: 14px;
  padding: 10px;
  outline: none;

  ${media.mobile`
    width: 100%;
  `}
`;
