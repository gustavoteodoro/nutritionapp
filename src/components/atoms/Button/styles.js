import styled from 'styled-components';
import { Primary } from '../../bosons/colors';
import { Roboto } from '../../bosons/fonts';
import { media } from '../../bosons/media';

export const ButtonInput = styled.button`
  width: 20%;
  height: 40px;
  background-color: ${Primary};
  font-size: 14px;
  padding: 10px;
  border: none;
  text-align: center;

  ${media.mobile`
    width: 100%;
  `}
`;

export const ButtonLabel = styled.span`
  font-family: ${Roboto};
  font-size: 14px;
  color: white;

  ${media.mobile`
    width: 100%;
  `}
`;
