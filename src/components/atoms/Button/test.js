import React from 'react';
import Button from './index';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer
    .create(<Button label="Search" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});