import React, { Component } from 'react';
import SkipLogo from '../../../assets/img/logo.png';

import {
  LogoContent,
} from './styles';

class Logo extends Component {
  render() {
    return (
      <LogoContent src={SkipLogo} width="150px" height="150px" alt="Skip Logo" />
    );
  }
}

export default Logo;
