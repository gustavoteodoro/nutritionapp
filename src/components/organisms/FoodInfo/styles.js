import styled from 'styled-components';

import { Primary, Secondary } from '../../bosons/colors';
import { Roboto } from '../../bosons/fonts';
import { media } from '../../bosons/media';

export const FoodInfoContainer = styled.article`
  display: flex;
  width: 100%;
  max-width: 600px;
  margin: 30px auto;
  flex-wrap: wrap;
  background: white;
  box-shadow: 3px 3px 5px #0003;

  ${media.mobile`
    margin: 0;
  `}
`;

export const FoodName = styled.h1`
  font-family: ${Roboto};
  color: ${Primary};
  font-size: 40px;
  font-weight: 600;
  margin-left: 20px;
`;

export const FoodInfoHeader = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
`;

export const FoodInfoThumb = styled.img`
  width: 100px;
  margin: 20px;
  border-radius: 5px;
  border: 2px solid ${Secondary};
  padding: 20px;
`;
