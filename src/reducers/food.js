import { SET_FOOD, CLEAR_FOOD } from '../actions/food';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case SET_FOOD: {
      return Object.assign({}, state, action.food);
    }
    case CLEAR_FOOD: {
      return {};
    }
    default:
      return state;
  }
}
